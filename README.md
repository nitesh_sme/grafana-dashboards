# Grafana JSON Dashboards

This repository contains sample dashboards that will display various GitLab
performance metrics obtained from InfluxDB. See [GitLab CE][gitlab-ce-docs] or
[GitLab EE][gitlab-ee-docs] for helping configuring/installing InfluxDB, Grafana
and these dashboards.

[gitlab-ce-docs]: http://docs.gitlab.com/ce/monitoring/performance/introduction.html
[gitlab-ee-docs]: http://docs.gitlab.com/ee/monitoring/performance/introduction.html
